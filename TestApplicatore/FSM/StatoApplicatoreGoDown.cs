﻿using NLog;

namespace TestApplicatore.FSM
{
    /// <summary>
    /// Stato in cui l'applicatore si trova in attesa di applicazione
    /// </summary>
    public class StatoApplicatoreGoDown : IStateApplicatore
    {
        /// <summary>
        /// Intervallo di tempo soglia prima che l'applicatore vada in errore [Sec]
        /// </summary>
        private const long IntervalloTempoErroreGoDown = 5000;

        /// <summary>
        /// Logger
        /// </summary>
        private Logger _logger = LogManager.GetCurrentClassLogger();

        /// <summary>
        /// StatoZero
        /// </summary>
        public StatoApplicatoreGoDown()
        {
            _logger.Trace("StatoApplicatoreGoDown inizialize");
        }

        /// <summary>
        /// State Name
        /// </summary>
        public string StateName
        {
            get
            {
                return "Apppliacatore Go Down";
            }
        }

        /// <summary>
        /// OnEnter
        /// </summary>
        public void OnEnter(Applicatore applicatore)
        {
            _logger.Trace("StatoApplicatoreGoDown OnEnter");
            applicatore.RelayApplicatoreUp = false;
            applicatore.RelayApplicatoreDown = true;
            applicatore.ApplicatoreOccupato = true;
            applicatore.ApplicazioneGoDownEtichettaTimer.Reset();
            applicatore.ApplicazioneGoDownEtichettaTimer.Start();
        }

        /// <summary>
        /// OnExit
        /// </summary>
        public void OnExit(Applicatore applicatore)
        {
            _logger.Trace("StatoApplicatoreGoDown OnExit");
            applicatore.RelayApplicatoreUp = false;
            applicatore.RelayApplicatoreDown = false;
            applicatore.ApplicatoreOccupato = true;
            applicatore.ApplicazioneGoDownEtichettaTimer.Reset();
        }

        /// <summary>
        /// Update
        /// </summary>
        public void Update(Applicatore applicatore)
        {
            _logger.Trace("StatoApplicatoreGoDown Update");
            if (!applicatore.Emmergenza)
            {
                applicatore.ChangeState(Applicatore.EnumStates.StatoApplcatoreGoUp);
                return;
            }

            if (applicatore.SensoreApplicatoreInferiore)
            {
                applicatore.RelayApplicatoreUp = false;
                applicatore.RelayApplicatoreDown = false;
                applicatore.ChangeState(Applicatore.EnumStates.StatoApplicaEttichetta);
                return;
            }

            //
            //  Gestione di un eventuale inceppamento
            //
            if (applicatore.ApplicazioneGoDownEtichettaTimer.ElapsedMilliseconds > IntervalloTempoErroreGoDown)
            {
                _logger.Error("L'applicatore non è riuscito a raggingere la posizione inferiore");
                applicatore.RelayApplicatoreUp = false;
                applicatore.RelayApplicatoreDown = false;
                applicatore.ErroreTimerGoDown = true;
                applicatore.ChangeState(Applicatore.EnumStates.StatoErrore);
                return;
            }
        }
    }
}