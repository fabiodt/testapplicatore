﻿using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace TestApplicatore.FSM
{
    /// <summary>
    /// Applicatore
    /// </summary>
    public class Applicatore
    {
        /// <summary>
        /// Applicazione Etichetta Timer
        /// </summary>
        public Stopwatch ApplicazioneEtichettaTimer = new Stopwatch();

        /// <summary>
        /// Applicazione Etichetta Timer
        /// </summary>
        public Stopwatch ApplicazioneAttesaStampaCompletaTimer = new Stopwatch();

        /// <summary>
        /// Applicazione Go Down Etichetta Timer
        /// </summary>
        public Stopwatch ApplicazioneGoDownEtichettaTimer = new Stopwatch();

        /// <summary>
        /// Applicazione Go Up Etichetta Timer
        /// </summary>
        public Stopwatch ApplicazioneGoUpEtichettaTimer = new Stopwatch();

        /// <summary>
        /// Lista degli stati
        /// </summary>
        public Dictionary<EnumStates, IStateApplicatore> states = new Dictionary<EnumStates, IStateApplicatore>();

        /// <summary>
        /// StateApplicatoe
        /// </summary>
        private IStateApplicatore _stateApplicatore = null;

        /// <summary>
        /// Stai dell'applicatore per un acesso veloce
        /// </summary>
        [Flags]
        public enum EnumStates
        {
            StatoApplcatoreGoUp = 0,
            StatoAttesaEttichettaVaschetta,
            StatoAttendiStampaCompleta,
            StatoApplicatoreGoDown,
            StatoApplicaEttichetta,
            StatoErrore,
        }

        /// <summary>
        /// Return lo stato dell'applicatore
        /// </summary>
        public string CurrentStateName => _stateApplicatore.StateName;

        /// <summary>
        /// Relay Applicatore Down
        /// </summary>
        public bool RelayApplicatoreDown { get; set; }

        /// <summary>
        /// Relay Applicatore Up
        /// </summary>
        public bool RelayApplicatoreUp { get; set; }

        /// <summary>
        /// Relay Vuoto Applicatore
        /// </summary>
        public bool RelayVuotoApplicatore { get; set; }

        /// <summary>
        /// Sensore Vaschetta Presente
        /// </summary>
        public bool SensoreVaschettaPresente { get; set; }

        /// <summary>
        /// Applicatore Occupato
        /// </summary>
        public bool ApplicatoreOccupato { get; set; }

        /// <summary>
        /// Emmergenza
        /// </summary>
        public bool Emmergenza { get; set; }

        /// <summary>
        /// Errore Timer Go Down (occorre quando ci mette troppo tempo a scendere il pistone)
        /// </summary>
        public bool ErroreTimerGoDown { get; set; } = false;

        /// <summary>
        /// ErrorTimerGoUp (occorre quando ci mette troppo tempo a scendere il pistone)
        /// </summary>
        public bool ErrorTimerGoUp { get; set; } = false;

        /// <summary>
        /// SensoreApplicatoreGiu
        /// </summary>
        public bool SensoreApplicatoreInferiore { get; set; }

        /// <summary>
        /// SensoreApplicatoreSu
        /// </summary>
        public bool SensoreApplicatoreSuperiore { get; set; }

        /// <summary>
        /// SensoreEttichettaPresente
        /// </summary>
        public bool SensoreEttichettaPresente { get; set; }

        /// <summary>
        /// Inizialize
        /// </summary>
        public void Inizialize()
        {
            states.Add(EnumStates.StatoApplcatoreGoUp, new StatoApplcatoreGoUp());
            states.Add(EnumStates.StatoAttesaEttichettaVaschetta, new StatoAttesaEttichettaVaschetta());
            states.Add(EnumStates.StatoAttendiStampaCompleta, new StatoAttendiStampaCompleta());
            states.Add(EnumStates.StatoApplicatoreGoDown, new StatoApplicatoreGoDown());
            states.Add(EnumStates.StatoApplicaEttichetta, new StatoApplicaEttichetta());
            states.Add(EnumStates.StatoErrore, new StatoErrore());
            //
            //  Faccio partire dallo stato go Up
            //
            _stateApplicatore = states[EnumStates.StatoApplcatoreGoUp];
            _stateApplicatore.OnEnter(this);
        }

        /// <summary>
        /// Update
        /// </summary>
        public void Update()
        {
            if (_stateApplicatore != null)
            {
                _stateApplicatore.Update(this);
            }
        }

        /// <summary>
        /// Change State
        /// </summary>
        /// <param name="newState"></param>
        public void ChangeState(EnumStates newState)
        {
            var state = states[newState];

            if (state == null)
            {
                return;
            }

            _stateApplicatore.OnExit(this);
            _stateApplicatore = state;
            _stateApplicatore.OnEnter(this);
        }
    }
}