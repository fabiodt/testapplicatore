﻿using NLog;

namespace TestApplicatore.FSM
{
    /// <summary>
    /// Stato in cui viene applicata l'ettichetta
    /// </summary>
    public class StatoApplicaEttichetta : IStateApplicatore
    {
        /// <summary>
        /// Intervallo di tempo soglia prima che l'applicatore vada in errore [Sec]
        /// </summary>
        private const int IntervalloTempoApplicaEttichetta = 2000;

        /// <summary>
        /// Logger
        /// </summary>
        private Logger _logger = LogManager.GetCurrentClassLogger();

        /// <summary>
        /// StatoZero
        /// </summary>
        public StatoApplicaEttichetta()
        {
            _logger.Trace("Stato Applica Ettichetta");
        }

        /// <summary>
        /// State Name
        /// </summary>
        public string StateName
        {
            get
            {
                return "Stato Applica Ettichetta";
            }
        }

        /// <summary>
        /// OnEnter
        /// </summary>
        public void OnEnter(Applicatore applicatore)
        {
            _logger.Trace("StatoApplicaEttichetta OnEnter");
            applicatore.ApplicazioneEtichettaTimer.Reset();
            applicatore.ApplicazioneEtichettaTimer.Start();
            applicatore.ApplicatoreOccupato = true;
            applicatore.RelayVuotoApplicatore = false;
        }

        /// <summary>
        /// OnExit
        /// </summary>
        public void OnExit(Applicatore applicatore)
        {
            _logger.Trace("StatoApplicaEttichetta OnExit");
            applicatore.ApplicazioneEtichettaTimer.Stop();
            applicatore.ApplicazioneEtichettaTimer.Reset();
            applicatore.ApplicatoreOccupato = true;
        }

        /// <summary>
        /// Update
        /// </summary>
        public void Update(Applicatore applicatore)
        {
            _logger.Trace("StatoApplicaEttichetta Update");

            if (applicatore.ApplicazioneEtichettaTimer.ElapsedMilliseconds > IntervalloTempoApplicaEttichetta && applicatore.Emmergenza)
            {
                applicatore.ChangeState(Applicatore.EnumStates.StatoApplcatoreGoUp);
            }
        }
    }
}