﻿using NLog;

namespace TestApplicatore.FSM
{
    public class StatoErrore : IStateApplicatore
    {
        /// <summary>
        /// Logger
        /// </summary>
        private Logger _logger = LogManager.GetCurrentClassLogger();

        /// <summary>
        /// Ritorna il nome dello stato
        /// </summary>
        public string StateName
        {
            get
            {
                return "Stato Errore";
            }
        }

        /// <summary>
        /// OnEnter
        /// </summary>
        /// <param name="applicatore"></param>
        public void OnEnter(Applicatore applicatore)
        {
            _logger.Trace("StatoErrore OnEnter");
        }

        /// <summary>
        /// OnExit
        /// </summary>
        /// <param name="applicatore"></param>
        public void OnExit(Applicatore applicatore)
        {
            _logger.Trace("StatoErrore OnExit");
        }

        /// <summary>
        /// Update
        /// </summary>
        /// <param name="applicatore"></param>
        public void Update(Applicatore applicatore)
        {
            _logger.Trace("StatoErrore Update");
            if (!applicatore.Emmergenza)
            {
                _logger.Error("L'applicatore non è riuscito a raggingere la posizione Superiore");

                applicatore.RelayApplicatoreUp = false;
                applicatore.RelayApplicatoreDown = false;
                applicatore.ErroreTimerGoDown = false;
                applicatore.ErrorTimerGoUp = false;
                applicatore.ChangeState(Applicatore.EnumStates.StatoApplcatoreGoUp);
                return;
            }
        }
    }
}