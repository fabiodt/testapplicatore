﻿using System;

namespace TestApplicatore.CBcom.Exceptions
{
    /// <summary>
    /// Check Sum Error Exception
    /// </summary>
    [Serializable]
    public class CheckSumErrorException : Exception
    {
    }
}