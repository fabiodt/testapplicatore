﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using System.Threading;
using TestApplicatore.CBcom.Exceptions;

namespace TestApplicatore.CBcom
{
    /// <summary>
    /// Modulo Analogioco CB COM 7016
    /// </summary>
    public class CbCom7016 : CbComGeneric
    {
        private byte[] _buffer = new byte[100];

        public CbCom7016(SerialPortWithLock serialPort, int address) : base(serialPort, address)
        {
            //
            //  Il modulo ha solo 4 canali relè
            //
            if ((address < 0) || (address > 0xFF))
            {
                throw new ArgumentOutOfRangeException();
            }

            if (!SerialPort.IsOpen)
            {
                SerialPort.Open();
            }
        }

        /// <summary>
        /// Get ADC Value
        /// </summary>
        /// <param name="channalN">Channel Number</param>
        /// <returns>Return anlogic value</returns>
        public float GetValue(int channalN)
        {
            if ((channalN < 0) || (channalN > 6))
            {
                throw new ArgumentOutOfRangeException();
            }

            string cmd = string.Format("#{0:X2}{0:X}", Address, channalN);

            List<byte> cmdBuffer = new List<byte>();
            cmdBuffer.AddRange(Encoding.ASCII.GetBytes(cmd));
            cmdBuffer.AddRange(Encoding.ASCII.GetBytes(CalcCheckSum(cmd)));
            cmdBuffer.Add(Convert.ToByte('\r'));
            SerialPort.Write(cmdBuffer.ToArray(), 0, cmdBuffer.Count);

            Thread.Sleep(50);
            SerialPort.Read(_buffer, 0, _buffer.Length);

            string fullRespoce = Encoding.ASCII.GetString(_buffer).Trim('\0', '\r');
            string responce = fullRespoce.Substring(0, fullRespoce.Length - 2);
            string checkSumString = fullRespoce.Substring(fullRespoce.Length - 2);

            if (fullRespoce.IndexOf("\rE", StringComparison.InvariantCulture) > -1)
            {
                throw new WrongMessageFormatException(cmdBuffer.ToArray(), _buffer);
            }

            // le ultime 3 cifre sono il controllo di crc
            uint checkSum = 0;
            foreach (byte c in Encoding.ASCII.GetBytes(responce))
            {
                checkSum += c;
            }

            checkSum = checkSum & 0xFF;
            if (checkSumString != checkSum.ToString("X"))
            {
                throw new CheckSumErrorException();
            }

            responce = responce.Trim('>');

            float value;
            if (!float.TryParse(responce, out value))
            {
                throw new WrongMessageFormatException(cmdBuffer.ToArray(), _buffer);
            }

            return value;
        }

        /// <summary>
        /// Get ADC Value
        /// </summary>
        /// <param name="channalN">Channel Number</param>
        /// <returns>Return anlogic value</returns>
        public float[] GetValues()
        {
            string cmd = string.Format("#{0:X2}", Address);

            List<byte> cmdBuffer = new List<byte>();
            cmdBuffer.AddRange(Encoding.ASCII.GetBytes(cmd));
            cmdBuffer.AddRange(Encoding.ASCII.GetBytes(CalcCheckSum(cmd)));
            cmdBuffer.Add(Convert.ToByte('\r'));
            SerialPort.Write(cmdBuffer.ToArray(), 0, cmdBuffer.Count);

            Thread.Sleep(100);
            SerialPort.Read(_buffer, 0, _buffer.Length);

            string fullRespoce = Encoding.ASCII.GetString(_buffer).Trim('\0', '\r');
            string responce = fullRespoce.Substring(0, fullRespoce.Length - 2);
            string checkSumString = fullRespoce.Substring(fullRespoce.Length - 2);

            if (fullRespoce.IndexOf("\rE", StringComparison.InvariantCulture) > -1)
            {
                throw new WrongMessageFormatException(cmdBuffer.ToArray(), _buffer);
            }

            // le ultime 3 cifre sono il controllo di crc
            uint checkSum = 0;
            foreach (byte c in Encoding.ASCII.GetBytes(responce))
            {
                checkSum += c;
            }

            checkSum = checkSum & 0xFF;
            if (checkSumString != checkSum.ToString("X"))
            {
                throw new CheckSumErrorException();
            }

            responce = responce.Trim('>');
            //
            //  La funzione è sbaglaita , non mappa i negativi
            //

            string[] responceSplit = responce.Split('+', '-');
            if (responceSplit.Length != 9)
            {
                throw new WrongMessageFormatException(cmdBuffer.ToArray(), _buffer);
            }

            var result = new float[responceSplit.Length - 1];
            for (int index = 1; index < responceSplit.Length; index++)
            {
                string str = responceSplit[index];
                float value;

                if (string.IsNullOrEmpty(str))
                {
                    continue;
                }

                if (!float.TryParse(str, NumberStyles.Any, CultureInfo.InvariantCulture, out value))
                {
                    throw new WrongMessageFormatException(cmdBuffer.ToArray(), _buffer);
                }
                result[index - 1] = value;
            }

            return result;
        }
    }
}