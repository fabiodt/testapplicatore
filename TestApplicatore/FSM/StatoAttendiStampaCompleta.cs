﻿using NLog;

namespace TestApplicatore.FSM
{
    /// <summary>
    /// Stato in cui attendo qualche secondo per la stampa completa dell'ettichetta
    /// </summary>
    public class StatoAttendiStampaCompleta : IStateApplicatore
    {
        /// <summary>
        /// Attesa Stampa Completa
        /// </summary>
        private const long AttesaStampaCompleta = 200;

        /// <summary>
        /// Logger
        /// </summary>
        private Logger _logger = LogManager.GetCurrentClassLogger();

        /// <summary>
        /// StatoZero
        /// </summary>
        public StatoAttendiStampaCompleta()
        {
            _logger.Trace("StatoAttendiStampaCompleta");
        }

        /// <summary>
        /// State Name
        /// </summary>
        public string StateName
        {
            get
            {
                return "Stato Attendi Stampa Completa";
            }
        }

        /// <summary>
        /// On Enter
        /// </summary>
        public void OnEnter(Applicatore applicatore)
        {
            _logger.Trace("StatoAttendiStampaCompleta OnEnter");
            applicatore.ApplicatoreOccupato = false;
            applicatore.ApplicazioneAttesaStampaCompletaTimer.Reset();
            applicatore.ApplicazioneAttesaStampaCompletaTimer.Start();
        }

        /// <summary>
        /// On Exit
        /// </summary>
        public void OnExit(Applicatore applicatore)
        {
            _logger.Trace("StatoAttendiStampaCompleta OnExit");
            applicatore.ApplicazioneAttesaStampaCompletaTimer.Reset();
            applicatore.ApplicatoreOccupato = false;
        }

        /// <summary>
        /// Update
        /// </summary>
        public void Update(Applicatore applicatore)
        {
            _logger.Trace("StatoAttendiStampaCompleta Update");

            if (!applicatore.Emmergenza)
            {
                applicatore.ChangeState(Applicatore.EnumStates.StatoApplicatoreGoDown);
                return;
            }

            if (applicatore.ApplicazioneAttesaStampaCompletaTimer.ElapsedMilliseconds > AttesaStampaCompleta)
            {
                applicatore.ChangeState(Applicatore.EnumStates.StatoApplicatoreGoDown);
            }
        }
    }
}