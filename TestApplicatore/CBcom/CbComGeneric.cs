﻿using System.Text;
using TestApplicatore.CBcom;

namespace TestApplicatore
{
    /// <summary>
    /// CbComGeneric abstract class
    /// </summary>
    public abstract class CbComGeneric
    {
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="address"></param>
        public CbComGeneric(SerialPortWithLock serialPortWithLock, int address)
        {
            Address = address;
            SerialPort = serialPortWithLock;
        }

        /// <summary>
        /// Module Address
        /// </summary>
        public int Address { private set; get; }

        /// <summary>
        /// Module Address
        /// </summary>
        public SerialPortWithLock SerialPort { get; set; }

        /// <summary>
        /// Calc check sum
        /// </summary>
        /// <param name="cmd"></param>
        /// <returns></returns>
        public static string CalcCheckSum(string cmd)
        {
            uint checkSum = 0;
            foreach (byte b in Encoding.ASCII.GetBytes(cmd))
            {
                checkSum += b;
            }

            checkSum = checkSum & 0xFF;
            return checkSum.ToString("X");
        }

        /// <summary>
        /// Svuota Buffer
        /// </summary>
        public void SvuotaBuffer()
        {
            try
            {
                byte[] buffer = new byte[100];
                SerialPort.Read(buffer, 0, buffer.Length);
            }
            catch
            {
            }
        }
    }
}