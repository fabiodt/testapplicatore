﻿using System;
using System.IO.Ports;
using System.Text;

namespace TestApplicatore.CBcom
{
    /// <summary>
    /// Serial Port With Lock
    /// </summary>
    public class SerialPortWithLock : IDisposable
    {
        /// <summary>
        /// Serial Port
        /// </summary>
        private readonly SerialPort _serialPort = new SerialPort();

        /// <summary>
        ///  Serial Port Lock
        /// </summary>
        private readonly object _serialPortLock = new object();

        /// <summary>
        /// Distruttore
        /// </summary>
        ~SerialPortWithLock()
        {
            lock (_serialPortLock)
            {
                if(_serialPort == null)
                {
                    return;
                }
                    
                if (_serialPort.IsOpen)
                {
                    _serialPort.Close();
                }
            }
        }


        /// <summary>
        /// Gets or sets the serial baud rate.
        /// </summary>
        public int BaudRate
        {
            get
            {
                lock (_serialPortLock)
                {
                    return _serialPort.BaudRate;
                }
            }

            set
            {
                lock (_serialPortLock)
                {
                    _serialPort.BaudRate = value;
                }
            }
        }

        /// <summary>
        /// Gets or sets the byte encoding for pre- and post-transmission conversion of text.
        /// </summary>
        public Encoding Encoding
        {
            get
            {
                lock (_serialPortLock)
                {
                    return _serialPort.Encoding;
                }
            }

            set
            {
                lock (_serialPortLock)
                {
                    _serialPort.Encoding = value;
                }
            }
        }

        /// <summary>
        /// Is Open
        /// </summary>
        public bool IsOpen
        {
            get
            {
                lock (_serialPortLock)
                {
                    return _serialPort.IsOpen;
                }
            }
        }

        /// <summary>
        /// Gets or sets the parity-checking protocol.
        /// </summary>
        public Parity Parity
        {
            get
            {
                lock (_serialPortLock)
                {
                    return _serialPort.Parity;
                }
            }

            set
            {
                lock (_serialPortLock)
                {
                    _serialPort.Parity = value;
                }
            }
        }

        /// <summary>
        /// Gets or sets the port for communications, including but not limited to all available
        /// COM ports.
        /// </summary>
        public string PortName
        {
            get
            {
                lock (_serialPortLock)
                {
                    return _serialPort.PortName;
                }
            }

            set
            {
                lock (_serialPortLock)
                {
                    _serialPort.PortName = value;
                }
            }
        }

        /// <summary>
        ///  Gets or sets the number of milliseconds before a time-out occurs when a read
        ///  operation does not finish.
        /// </summary>
        public int ReadTimeout
        {
            get
            {
                lock (_serialPortLock)
                {
                    return _serialPort.ReadTimeout;
                }
            }

            set
            {
                lock (_serialPortLock)
                {
                    _serialPort.ReadTimeout = value;
                }
            }
        }

        /// <summary>
        /// Gets or sets the standard number of stopbits per byte.
        /// </summary>
        public StopBits StopBits
        {
            get
            {
                lock (_serialPortLock)
                {
                    return _serialPort.StopBits;
                }
            }

            set
            {
                lock (_serialPortLock)
                {
                    _serialPort.StopBits = value;
                }
            }
        }

        /// <summary>
        /// Close
        /// </summary>
        public void Close()
        {
            lock (_serialPortLock)
            {
                _serialPort.Close();
            }
        }

        /// <summary>
        /// Dispose resource
        /// </summary>
        public void Dispose()
        {
            lock (_serialPortLock)
            {
                if (_serialPort == null)
                {
                    return;
                }

                if (_serialPort.IsOpen)
                {
                    _serialPort.Close();
                }
            }
        }

        /// <summary>
        /// Open
        /// </summary>
        public void Open()
        {
            lock (_serialPortLock)
            {
                _serialPort.Open();
            }
        }

        /// <summary>
        /// Read
        /// </summary>
        /// <param name="buffer"></param>
        /// <param name="offset"></param>
        /// <param name="count"></param>
        public int Read(byte[] buffer, int offset, int count)
        {
            lock (_serialPortLock)
            {
                return _serialPort.Read(buffer, offset, count);
            }
        }

        /// <summary>
        /// Write
        /// </summary>
        /// <param name="buffer"></param>
        /// <param name="offset"></param>
        /// <param name="count"></param>
        public void Write(byte[] buffer, int offset, int count)
        {
            lock (_serialPortLock)
            {
                _serialPort.Write(buffer, offset, count);
            }
        }
    }
}