﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using TestApplicatore.CBcom.Exceptions;

namespace TestApplicatore.CBcom
{
    /// <summary>
    /// Implementazione del modulo CB 7060
    /// </summary>
    internal class CbCom7060 : CbComGeneric
    {
        private byte[] _buffer = new byte[100];

        public CbCom7060(SerialPortWithLock serialPort, int address) : base(serialPort, address)
        {
            //
            //  Il modulo ha solo 4 canali relè
            //
            if ((address < 0) || (address > 0xFF))
            {
                throw new ArgumentOutOfRangeException();
            }
        }

        public void SetValue(int address, int channalN, bool state)
        {
            if (!SerialPort.IsOpen)
            {
                SerialPort.Open();
            }

            //
            //  Il modulo ha solo 4 canali relè
            //
            if ((address < 0) || (address > 0xFF))
            {
                throw new ArgumentOutOfRangeException();
            }

            //
            //  Il modulo ha solo 4 canali relè
            //
            if ((channalN < 0) || (channalN > 3))
            {
                throw new ArgumentOutOfRangeException();
            }

            string cmd = string.Format("#{0:X2}A{1:X1}{2:02}", address, channalN, state ? "01" : "00");

            List<byte> cmdBuffer = new List<byte>();
            cmdBuffer.AddRange(Encoding.ASCII.GetBytes(cmd));
            cmdBuffer.AddRange(Encoding.ASCII.GetBytes(CalcCheckSum(cmd)));
            cmdBuffer.Add(Convert.ToByte('\r'));
            SerialPort.Write(cmdBuffer.ToArray(), 0, cmdBuffer.Count);

            Thread.Sleep(100);
            SerialPort.Read(_buffer, 0, _buffer.Length);

            string fullRespoce = Encoding.ASCII.GetString(_buffer).Trim('\0', '\r');
            string responce = fullRespoce.Substring(0, fullRespoce.Length - 2);
            string checkSumString = fullRespoce.Substring(fullRespoce.Length - 2);

            if (fullRespoce.IndexOf("\r?", StringComparison.InvariantCulture) > -1)
            {
                throw new WrongMessageFormatException(cmdBuffer.ToArray(), _buffer);
            }
        }
    }
}