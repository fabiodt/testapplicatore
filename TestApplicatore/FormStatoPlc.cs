﻿using NLog;
using System;
using System.IO.Ports;
using System.Text;
using System.Windows.Forms;
using TestApplicatore.CBcom;
using TestApplicatore.FSM;

namespace TestApplicatore
{
    public partial class FormStatoPlc : Form
    {
        private CbCom7016 _comIn;

        private CbCom7043 _comOut;

        /// <summary>
        /// Logger
        /// </summary>
        private Logger _logger = LogManager.GetCurrentClassLogger();

        /// <summary>
        /// SerialPortWithLock
        /// </summary>
        private SerialPortWithLock _serialPortWithLock = new SerialPortWithLock();

        /// <summary>
        /// FSM
        /// </summary>
        private Applicatore applicatore = new Applicatore();

        /// <summary>
        /// Constructor
        /// </summary>
        public FormStatoPlc()
        {
            InitializeComponent();
        }

        /// <summary>
        /// backgroundWorker DoWork, main loop for pooling IO
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void backgroundWorker1_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            while (!backgroundWorker1.CancellationPending)
            {
                //
                //  leggo e aggiorno gli in
                //
                var inData = _comIn.GetValues();

                applicatore.SensoreApplicatoreSuperiore = inData[0] > 7.0f;
                applicatore.SensoreApplicatoreInferiore = inData[1] > 7.0f;
                applicatore.SensoreEttichettaPresente = inData[2] > 7.0f;
                applicatore.SensoreVaschettaPresente = true;
                applicatore.Emmergenza = true;

                //
                //  aggiorno la logica
                //
                applicatore.Update();

                //
                //  setto gli out
                //
                _comOut.SetValue(0, applicatore.RelayApplicatoreUp);
                _comOut.SetValue(1, applicatore.RelayApplicatoreDown);
                _comOut.SetValue(2, applicatore.RelayVuotoApplicatore);
            }
        }

        /// <summary>
        /// Error handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void backgroundWorker1_RunWorkerCompleted(object sender, System.ComponentModel.RunWorkerCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                _logger.Error(e.Error);
                MessageBox.Show("Errore critico:" + e.Error.Message);
            }
        }

        /// <summary>
        /// Avvia il sistema in automatico
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonAutomatico_Click(object sender, EventArgs e)
        {
            if (backgroundWorker1.IsBusy)
            {
                backgroundWorker1.CancelAsync();
            }
            else
            {
                

                backgroundWorker1.RunWorkerAsync();
            }
        }

        /// <summary>
        /// Botton close
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonInput1Emmergenza_Click(object sender, EventArgs e)
        {
            applicatore.Emmergenza = !applicatore.Emmergenza;
            applicatore.Update();
            UpdateUI();
        }

        /// <summary>
        /// Button Input 2 ApplicatorePosSuperiore
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonInput2ApplicatorePosSuperiore_Click(object sender, EventArgs e)
        {
            applicatore.SensoreApplicatoreSuperiore = !applicatore.SensoreApplicatoreSuperiore;

            applicatore.Update();
            UpdateUI();
        }

        /// <summary>
        /// Button Input 3 Applicatore Pos Inf
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonInput3ApplicatorePosInf_Click(object sender, EventArgs e)
        {
            applicatore.SensoreApplicatoreInferiore = !applicatore.SensoreApplicatoreInferiore;

            applicatore.Update();
            UpdateUI();
        }

        /// <summary>
        /// button Input 4 Ettichetta Presente
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonInput4EttichettaPresente_Click(object sender, EventArgs e)
        {
            applicatore.SensoreEttichettaPresente = !applicatore.SensoreEttichettaPresente;

            applicatore.Update();
            UpdateUI();
        }

        /// <summary>
        /// button Input 5 Vaschetta Presente
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonInput5VaschettaPresente_Click(object sender, EventArgs e)
        {
            applicatore.SensoreVaschettaPresente = !applicatore.SensoreVaschettaPresente;

            applicatore.Update();
            UpdateUI();
        }

        /// <summary>
        /// button Relay 5 Applicatore Down Click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonRelay5ApplicatoreDown_Click(object sender, EventArgs e)
        {
        }

        private void buttonRelay6Allarme_Click(object sender, EventArgs e)
        {
        }

        private void buttonRelay6ApplicatoreUp_Click(object sender, EventArgs e)
        {
        }

        /// <summary>
        /// Button Relay 7 Vuoto Applicatore Click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonRelay7VuotoApplicatore_Click(object sender, EventArgs e)
        {
        }

        /// <summary>
        /// button Test IO Click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonTestIO_Click(object sender, EventArgs e)
        {
            //try
            //{
            //}
            //catch (Exception ex)
            //{
            //    MessageBox.Show(ex.Message);
            //    _logger.Error(ex);
            //}
        }

        private bool DeleteMe(bool stato, CbCom7016 comIn, CbCom7043 com)
        {
            //var xxxx = comIn.GetValue(0);

            //var x2 = comIn.GetValue(1);
            // x2 = comIn.GetValue(2);
            // x2 = comIn.GetValue(3);
            // x2 = comIn.GetValue(4);
            // x2 = comIn.GetValue(5);
            //x2 = comIn.GetValue(6);
            //x2 = comIn.GetValue(5);
            com.SetValue(1, stato);
            com.SetValue(2, stato);
            stato = !stato;
            return stato;
        }

        /// <summary>
        /// Load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FormStatoPlc_Load(object sender, EventArgs e)
        {
            try
            {
                if (_serialPortWithLock.IsOpen)
                {
                    _serialPortWithLock.Close();
                }
                _serialPortWithLock.PortName = Properties.Settings.Default.PortName;
                _serialPortWithLock.BaudRate = 9600;
                _serialPortWithLock.Encoding = Encoding.ASCII;
                _serialPortWithLock.StopBits = StopBits.One;
                _serialPortWithLock.Parity = Parity.None;
                _serialPortWithLock.ReadTimeout = 100;

                _comIn = new CbCom7016(_serialPortWithLock, 1);
                _comOut = new CbCom7043(_serialPortWithLock, 0);

                _comIn.SvuotaBuffer();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                _logger.Error(ex);
            }

            applicatore.Inizialize();
            applicatore.Emmergenza = true;
            applicatore.Update();
            UpdateUI();
        }

        /// <summary>
        /// Timer update
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void timer1_Tick(object sender, EventArgs e)
        {


            ////
            ////  leggo e aggiorno gli in
            ////
            //var inData = _comIn.GetValues();

            //applicatore.SensoreApplicatoreSuperiore = inData[0] > 1.0f;
            //applicatore.SensoreApplicatoreInferiore = inData[1] > 1.0f;
            //applicatore.SensoreEttichettaPresente = inData[2] > 1.0f;
            //applicatore.SensoreVaschettaPresente = true;
            //applicatore.Emmergenza = true;

            //
            //  aggiorno la logica
            ////
            //applicatore.Update();

            ////
            ////  setto gli out
            ////
            //_comOut.SetValue(0, applicatore.RelayApplicatoreUp);
            //_comOut.SetValue(1, applicatore.RelayApplicatoreDown);
            //_comOut.SetValue(2, applicatore.RelayVuotoApplicatore);


            applicatore.Update();
            UpdateUI();
        }

        /// <summary>
        /// Update Ui
        /// </summary>
        private void UpdateUI()
        {
            buttonInput1Emmergenza.BackColor = applicatore.Emmergenza ? System.Drawing.Color.Red : buttonClose.BackColor;
            buttonInput2ApplicatorePosSuperiore.BackColor = applicatore.SensoreApplicatoreSuperiore ? System.Drawing.Color.Red : buttonClose.BackColor;
            buttonInput3ApplicatorePosInf.BackColor = applicatore.SensoreApplicatoreInferiore ? System.Drawing.Color.Red : buttonClose.BackColor;
            buttonInput4EttichettaPresente.BackColor = applicatore.SensoreEttichettaPresente ? System.Drawing.Color.Red : buttonClose.BackColor;
            buttonInput5VaschettPresente.BackColor = applicatore.SensoreVaschettaPresente ? System.Drawing.Color.Red : buttonClose.BackColor;

            buttonRelay5ApplicatoreDown.BackColor = applicatore.RelayApplicatoreDown ? System.Drawing.Color.Red : buttonClose.BackColor;
            buttonRelay6ApplicatoreUp.BackColor = applicatore.RelayApplicatoreUp ? System.Drawing.Color.Red : buttonClose.BackColor;
            buttonRelay7VuotoApplicatore.BackColor = applicatore.RelayVuotoApplicatore ? System.Drawing.Color.Red : buttonClose.BackColor;
            //
            //  Allarmi
            //
            bool allarme = false;
            allarme |= applicatore.ErroreTimerGoDown;
            allarme |= applicatore.ErrorTimerGoUp;

            buttonRelay6Allarme.BackColor = allarme ? System.Drawing.Color.Red : buttonClose.BackColor;

            //
            //  Virtual relay
            //
            buttonVRErroreTimerGoDown.BackColor = applicatore.ErroreTimerGoDown ? System.Drawing.Color.Red : buttonClose.BackColor;
            buttonVRErroreTimerGoUp.BackColor = applicatore.ErrorTimerGoUp ? System.Drawing.Color.Red : buttonClose.BackColor;
            buttonVRApplicatoreBusy.BackColor = applicatore.ApplicatoreOccupato ? System.Drawing.Color.Red : buttonClose.BackColor;

            buttonAutomatico.BackColor = backgroundWorker1.IsBusy ? System.Drawing.Color.Red : buttonClose.BackColor;

            labelState.Text = applicatore.CurrentStateName;

            labelTimerApplicazione.Text = string.Format("Timer Applicazione {0:0.00} sec", applicatore.ApplicazioneEtichettaTimer.Elapsed.TotalSeconds);
            labelGoUpTimer.Text = string.Format("Timer  Go Up Timer {0:0.00} sec", applicatore.ApplicazioneGoUpEtichettaTimer.Elapsed.TotalSeconds);
            labelGoDownTimer.Text = string.Format("Timer Go Down Timer {0:0.00} sec", applicatore.ApplicazioneGoDownEtichettaTimer.Elapsed.TotalSeconds);
            labelStampaCompletaTimer.Text = string.Format("Timer Attesa stampa completa {0:0.00} sec", applicatore.ApplicazioneAttesaStampaCompletaTimer.Elapsed.TotalSeconds);
        }
        /// <summary>
        /// FormStatoPlc FormClosing
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FormStatoPlc_FormClosing(object sender, FormClosingEventArgs e)
        {
            backgroundWorker1.CancelAsync();
        }
        /// <summary>
        /// FormStatoPlc FormClosed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FormStatoPlc_FormClosed(object sender, FormClosedEventArgs e)
        {
            backgroundWorker1.CancelAsync();
        }
    }
}