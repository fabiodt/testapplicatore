﻿using NLog;

namespace TestApplicatore.FSM
{
    /// <summary>
    /// Stato in cui l'applicatore si trova in attesa di applicazione
    /// </summary>
    public class StatoAttesaEttichettaVaschetta : IStateApplicatore
    {
        /// <summary>
        /// Logger
        /// </summary>
        private Logger _logger = LogManager.GetCurrentClassLogger();

        /// <summary>
        /// StatoZero
        /// </summary>
        public StatoAttesaEttichettaVaschetta()
        {
            _logger.Trace("Stato Attesa Ettichetta e Vaschetta");
        }

        /// <summary>
        /// State Name
        /// </summary>
        public string StateName
        {
            get
            {
                return "Stato Attesa Ettichetta e Vaschetta";
            }
        }

        /// <summary>
        /// On Enter
        /// </summary>
        public void OnEnter(Applicatore applicatore)
        {
            _logger.Trace("StatoAttesaEttichettaVaschetta OnEnter");
            applicatore.ApplicatoreOccupato = false;
            applicatore.RelayVuotoApplicatore = true;
        }

        /// <summary>
        /// On Exit
        /// </summary>
        public void OnExit(Applicatore applicatore)
        {
            _logger.Trace("StatoAttesaEttichettaVaschetta OnExit");
            applicatore.ApplicatoreOccupato = false;
        }

        /// <summary>
        /// Update
        /// </summary>
        public void Update(Applicatore applicatore)
        {
            _logger.Trace("StatoAttesaEttichettaVaschetta Update");
            //
            //  Se è premuta l'emmergenza segnala l'applicatore occupato
            //
            applicatore.ApplicatoreOccupato = !applicatore.Emmergenza;

            //
            //
            //
            if (applicatore.SensoreApplicatoreSuperiore 
                && applicatore.SensoreEttichettaPresente 
                && applicatore.SensoreVaschettaPresente 
                && applicatore.Emmergenza)
            {
                applicatore.ChangeState(Applicatore.EnumStates.StatoAttendiStampaCompleta);
            }
        }
    }
}