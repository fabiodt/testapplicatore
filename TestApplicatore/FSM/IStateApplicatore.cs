﻿namespace TestApplicatore.FSM
{
    public interface IStateApplicatore
    {
        /// <summary>
        /// Ritorna il nome dello stato
        /// </summary>
        string StateName { get; }

        /// <summary>
        /// On Enter (inizializza lo stato)
        /// </summary>
        void OnEnter(Applicatore applicatore);

        /// <summary>
        /// OnvExit (finalizza lo stato)
        /// </summary>
        void OnExit(Applicatore applicatore);

        /// <summary>
        /// Update (main state loop)
        /// </summary>
        void Update(Applicatore applicatore);
    }
}