﻿using NLog;

namespace TestApplicatore.FSM
{
    public class StatoApplcatoreGoUp : IStateApplicatore
    {
        /// <summary>
        /// Intervallo di tempo soglia prima che l'applicatore vada in errore [mSec]
        /// </summary>
        private const long IntervalloTempoErroreGoUp = 30000;

        /// <summary>
        /// Logger
        /// </summary>
        private Logger _logger = LogManager.GetCurrentClassLogger();

        /// <summary>
        /// StatoZero
        /// </summary>
        public StatoApplcatoreGoUp()
        {
            _logger.Trace("Stato Applicatore Go Up");
        }

        /// <summary>
        /// State Name
        /// </summary>
        public string StateName
        {
            get
            {
                return "Stato Applicatore Go Up";
            }
        }

        /// <summary>
        /// OnEnter
        /// </summary>
        public void OnEnter(Applicatore applicatore)
        {
            _logger.Trace("StatoApplcatoreGoUp OnEnter");
            applicatore.ApplicazioneGoUpEtichettaTimer.Reset();
            applicatore.ApplicazioneGoUpEtichettaTimer.Start();
            applicatore.ApplicatoreOccupato = true;
        }

        /// <summary>
        /// OnExit
        /// </summary>
        public void OnExit(Applicatore applicatore)
        {
            _logger.Trace("StatoApplcatoreGoUp OnExit");
            applicatore.RelayApplicatoreUp = false;
            applicatore.RelayApplicatoreDown = false;
            applicatore.ApplicatoreOccupato = true;
            applicatore.ApplicazioneGoUpEtichettaTimer.Reset();
        }

        /// <summary>
        /// Update
        /// </summary>
        public void Update(Applicatore applicatore)
        {
            _logger.Trace("StatoApplcatoreGoUp Update");

            if (!applicatore.Emmergenza)
            {
                //
                //  se sgancio l'emmergenza si deve fermare
                //
                if (applicatore.RelayApplicatoreUp != false)
                {
                    applicatore.RelayApplicatoreUp = false;
                }
                if (applicatore.RelayApplicatoreDown != false)
                {
                    applicatore.RelayApplicatoreDown = false;
                }

                if (applicatore.ApplicazioneGoUpEtichettaTimer.IsRunning)
                {
                    applicatore.ApplicazioneGoUpEtichettaTimer.Stop();
                }
                return;
            }
            else
            {
                //
                //  forzo la risalita
                //
                if (applicatore.RelayApplicatoreUp != true)
                {
                    applicatore.RelayApplicatoreUp = true;
                }
                if (applicatore.RelayApplicatoreDown != false)
                {
                    applicatore.RelayApplicatoreDown = false;
                }
                if (!applicatore.ApplicazioneGoUpEtichettaTimer.IsRunning)
                {
                    applicatore.ApplicazioneGoUpEtichettaTimer.Start();
                }
            }
            //
            //  se il sensore è già in posizione passo al ciclo dopo
            //
            if (applicatore.SensoreApplicatoreSuperiore && applicatore.Emmergenza)
            {
                applicatore.ChangeState(Applicatore.EnumStates.StatoAttesaEttichettaVaschetta);
                return;
            }

            //
            //  Gestione di un eventuale inceppamento
            //
            if (applicatore.ApplicazioneGoUpEtichettaTimer.ElapsedMilliseconds > IntervalloTempoErroreGoUp)
            {
                _logger.Error("L'applicatore non è riuscito a raggingere la posizione Superiore");

                applicatore.RelayApplicatoreUp = false;
                applicatore.RelayApplicatoreDown = false;
                applicatore.ErrorTimerGoUp = true;
                applicatore.ChangeState(Applicatore.EnumStates.StatoErrore);
                return;
            }
        }
    }
}