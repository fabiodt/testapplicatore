﻿namespace TestApplicatore
{
    partial class FormStatoPlc
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.buttonInput1Emmergenza = new System.Windows.Forms.Button();
            this.buttonInput2ApplicatorePosSuperiore = new System.Windows.Forms.Button();
            this.buttonInput3ApplicatorePosInf = new System.Windows.Forms.Button();
            this.buttonInput4EttichettaPresente = new System.Windows.Forms.Button();
            this.buttonInput5VaschettPresente = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.buttonRelay5ApplicatoreDown = new System.Windows.Forms.Button();
            this.buttonRelay6ApplicatoreUp = new System.Windows.Forms.Button();
            this.buttonClose = new System.Windows.Forms.Button();
            this.labelState = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.labelTimerApplicazione = new System.Windows.Forms.Label();
            this.labelGoDownTimer = new System.Windows.Forms.Label();
            this.labelGoUpTimer = new System.Windows.Forms.Label();
            this.buttonRelay6Allarme = new System.Windows.Forms.Button();
            this.buttonVRApplicatoreBusy = new System.Windows.Forms.Button();
            this.buttonVRErroreTimerGoUp = new System.Windows.Forms.Button();
            this.buttonVRErroreTimerGoDown = new System.Windows.Forms.Button();
            this.buttonTestIO = new System.Windows.Forms.Button();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.buttonAutomatico = new System.Windows.Forms.Button();
            this.buttonRelay7VuotoApplicatore = new System.Windows.Forms.Button();
            this.labelStampaCompletaTimer = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // buttonInput1Emmergenza
            // 
            this.buttonInput1Emmergenza.Location = new System.Drawing.Point(12, 49);
            this.buttonInput1Emmergenza.Name = "buttonInput1Emmergenza";
            this.buttonInput1Emmergenza.Size = new System.Drawing.Size(173, 23);
            this.buttonInput1Emmergenza.TabIndex = 0;
            this.buttonInput1Emmergenza.Text = "1) Emmergenza";
            this.buttonInput1Emmergenza.UseVisualStyleBackColor = true;
            this.buttonInput1Emmergenza.Click += new System.EventHandler(this.buttonInput1Emmergenza_Click);
            // 
            // buttonInput2ApplicatorePosSuperiore
            // 
            this.buttonInput2ApplicatorePosSuperiore.Location = new System.Drawing.Point(12, 78);
            this.buttonInput2ApplicatorePosSuperiore.Name = "buttonInput2ApplicatorePosSuperiore";
            this.buttonInput2ApplicatorePosSuperiore.Size = new System.Drawing.Size(173, 23);
            this.buttonInput2ApplicatorePosSuperiore.TabIndex = 1;
            this.buttonInput2ApplicatorePosSuperiore.Text = "2) Applicatore Pos Superiore";
            this.buttonInput2ApplicatorePosSuperiore.UseVisualStyleBackColor = true;
            this.buttonInput2ApplicatorePosSuperiore.Click += new System.EventHandler(this.buttonInput2ApplicatorePosSuperiore_Click);
            // 
            // buttonInput3ApplicatorePosInf
            // 
            this.buttonInput3ApplicatorePosInf.Location = new System.Drawing.Point(12, 107);
            this.buttonInput3ApplicatorePosInf.Name = "buttonInput3ApplicatorePosInf";
            this.buttonInput3ApplicatorePosInf.Size = new System.Drawing.Size(173, 23);
            this.buttonInput3ApplicatorePosInf.TabIndex = 2;
            this.buttonInput3ApplicatorePosInf.Text = "3) Applicatore Pos Inf";
            this.buttonInput3ApplicatorePosInf.UseVisualStyleBackColor = true;
            this.buttonInput3ApplicatorePosInf.Click += new System.EventHandler(this.buttonInput3ApplicatorePosInf_Click);
            // 
            // buttonInput4EttichettaPresente
            // 
            this.buttonInput4EttichettaPresente.Location = new System.Drawing.Point(12, 136);
            this.buttonInput4EttichettaPresente.Name = "buttonInput4EttichettaPresente";
            this.buttonInput4EttichettaPresente.Size = new System.Drawing.Size(173, 23);
            this.buttonInput4EttichettaPresente.TabIndex = 3;
            this.buttonInput4EttichettaPresente.Text = "4) Ettichetta Presente";
            this.buttonInput4EttichettaPresente.UseVisualStyleBackColor = true;
            this.buttonInput4EttichettaPresente.Click += new System.EventHandler(this.buttonInput4EttichettaPresente_Click);
            // 
            // buttonInput5VaschettPresente
            // 
            this.buttonInput5VaschettPresente.Location = new System.Drawing.Point(12, 165);
            this.buttonInput5VaschettPresente.Name = "buttonInput5VaschettPresente";
            this.buttonInput5VaschettPresente.Size = new System.Drawing.Size(173, 23);
            this.buttonInput5VaschettPresente.TabIndex = 4;
            this.buttonInput5VaschettPresente.Text = "5) Vaschetta presente";
            this.buttonInput5VaschettPresente.UseVisualStyleBackColor = true;
            this.buttonInput5VaschettPresente.Click += new System.EventHandler(this.buttonInput5VaschettaPresente_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(43, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "Ingressi";
            // 
            // buttonRelay5ApplicatoreDown
            // 
            this.buttonRelay5ApplicatoreDown.Location = new System.Drawing.Point(683, 49);
            this.buttonRelay5ApplicatoreDown.Name = "buttonRelay5ApplicatoreDown";
            this.buttonRelay5ApplicatoreDown.Size = new System.Drawing.Size(152, 23);
            this.buttonRelay5ApplicatoreDown.TabIndex = 6;
            this.buttonRelay5ApplicatoreDown.Text = "5) Rele Applicatore Giu";
            this.buttonRelay5ApplicatoreDown.UseVisualStyleBackColor = true;
            this.buttonRelay5ApplicatoreDown.Click += new System.EventHandler(this.buttonRelay5ApplicatoreDown_Click);
            // 
            // buttonRelay6ApplicatoreUp
            // 
            this.buttonRelay6ApplicatoreUp.Location = new System.Drawing.Point(683, 78);
            this.buttonRelay6ApplicatoreUp.Name = "buttonRelay6ApplicatoreUp";
            this.buttonRelay6ApplicatoreUp.Size = new System.Drawing.Size(152, 23);
            this.buttonRelay6ApplicatoreUp.TabIndex = 7;
            this.buttonRelay6ApplicatoreUp.Text = "6) Rele Applicatore Su";
            this.buttonRelay6ApplicatoreUp.UseVisualStyleBackColor = true;
            this.buttonRelay6ApplicatoreUp.Click += new System.EventHandler(this.buttonRelay6ApplicatoreUp_Click);
            // 
            // buttonClose
            // 
            this.buttonClose.Location = new System.Drawing.Point(683, 498);
            this.buttonClose.Name = "buttonClose";
            this.buttonClose.Size = new System.Drawing.Size(173, 23);
            this.buttonClose.TabIndex = 8;
            this.buttonClose.Text = "Close";
            this.buttonClose.UseVisualStyleBackColor = true;
            this.buttonClose.Click += new System.EventHandler(this.buttonClose_Click);
            // 
            // labelState
            // 
            this.labelState.AutoSize = true;
            this.labelState.Location = new System.Drawing.Point(271, 9);
            this.labelState.Name = "labelState";
            this.labelState.Size = new System.Drawing.Size(32, 13);
            this.labelState.TabIndex = 9;
            this.labelState.Text = "Stato";
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // labelTimerApplicazione
            // 
            this.labelTimerApplicazione.AutoSize = true;
            this.labelTimerApplicazione.Location = new System.Drawing.Point(271, 83);
            this.labelTimerApplicazione.Name = "labelTimerApplicazione";
            this.labelTimerApplicazione.Size = new System.Drawing.Size(96, 13);
            this.labelTimerApplicazione.TabIndex = 11;
            this.labelTimerApplicazione.Text = "Timer Applicazione";
            // 
            // labelGoDownTimer
            // 
            this.labelGoDownTimer.AutoSize = true;
            this.labelGoDownTimer.Location = new System.Drawing.Point(271, 112);
            this.labelGoDownTimer.Name = "labelGoDownTimer";
            this.labelGoDownTimer.Size = new System.Drawing.Size(156, 13);
            this.labelGoDownTimer.TabIndex = 12;
            this.labelGoDownTimer.Text = "Timer Applicazione Down Timer";
            // 
            // labelGoUpTimer
            // 
            this.labelGoUpTimer.AutoSize = true;
            this.labelGoUpTimer.Location = new System.Drawing.Point(271, 141);
            this.labelGoUpTimer.Name = "labelGoUpTimer";
            this.labelGoUpTimer.Size = new System.Drawing.Size(142, 13);
            this.labelGoUpTimer.TabIndex = 13;
            this.labelGoUpTimer.Text = "Timer Applicazione Up Timer";
            // 
            // buttonRelay6Allarme
            // 
            this.buttonRelay6Allarme.Location = new System.Drawing.Point(683, 165);
            this.buttonRelay6Allarme.Name = "buttonRelay6Allarme";
            this.buttonRelay6Allarme.Size = new System.Drawing.Size(152, 23);
            this.buttonRelay6Allarme.TabIndex = 14;
            this.buttonRelay6Allarme.Text = "12) Allarme";
            this.buttonRelay6Allarme.UseVisualStyleBackColor = true;
            this.buttonRelay6Allarme.Click += new System.EventHandler(this.buttonRelay6Allarme_Click);
            // 
            // buttonVRApplicatoreBusy
            // 
            this.buttonVRApplicatoreBusy.Location = new System.Drawing.Point(378, 370);
            this.buttonVRApplicatoreBusy.Name = "buttonVRApplicatoreBusy";
            this.buttonVRApplicatoreBusy.Size = new System.Drawing.Size(152, 23);
            this.buttonVRApplicatoreBusy.TabIndex = 15;
            this.buttonVRApplicatoreBusy.Text = "V) Applicatore Buy";
            this.buttonVRApplicatoreBusy.UseVisualStyleBackColor = true;
            // 
            // buttonVRErroreTimerGoUp
            // 
            this.buttonVRErroreTimerGoUp.Location = new System.Drawing.Point(378, 341);
            this.buttonVRErroreTimerGoUp.Name = "buttonVRErroreTimerGoUp";
            this.buttonVRErroreTimerGoUp.Size = new System.Drawing.Size(152, 23);
            this.buttonVRErroreTimerGoUp.TabIndex = 16;
            this.buttonVRErroreTimerGoUp.Text = "V) Allarme Go Up";
            this.buttonVRErroreTimerGoUp.UseVisualStyleBackColor = true;
            // 
            // buttonVRErroreTimerGoDown
            // 
            this.buttonVRErroreTimerGoDown.Location = new System.Drawing.Point(378, 312);
            this.buttonVRErroreTimerGoDown.Name = "buttonVRErroreTimerGoDown";
            this.buttonVRErroreTimerGoDown.Size = new System.Drawing.Size(152, 23);
            this.buttonVRErroreTimerGoDown.TabIndex = 17;
            this.buttonVRErroreTimerGoDown.Text = "V) Allarme Go Down";
            this.buttonVRErroreTimerGoDown.UseVisualStyleBackColor = true;
            // 
            // buttonTestIO
            // 
            this.buttonTestIO.Location = new System.Drawing.Point(654, 340);
            this.buttonTestIO.Name = "buttonTestIO";
            this.buttonTestIO.Size = new System.Drawing.Size(75, 23);
            this.buttonTestIO.TabIndex = 18;
            this.buttonTestIO.Text = "Test IO";
            this.buttonTestIO.UseVisualStyleBackColor = true;
            this.buttonTestIO.Click += new System.EventHandler(this.buttonTestIO_Click);
            // 
            // backgroundWorker1
            // 
            this.backgroundWorker1.WorkerSupportsCancellation = true;
            this.backgroundWorker1.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorker1_DoWork);
            this.backgroundWorker1.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.backgroundWorker1_RunWorkerCompleted);
            // 
            // buttonAutomatico
            // 
            this.buttonAutomatico.Location = new System.Drawing.Point(12, 340);
            this.buttonAutomatico.Name = "buttonAutomatico";
            this.buttonAutomatico.Size = new System.Drawing.Size(173, 23);
            this.buttonAutomatico.TabIndex = 19;
            this.buttonAutomatico.Text = "Automatico";
            this.buttonAutomatico.UseVisualStyleBackColor = true;
            this.buttonAutomatico.Click += new System.EventHandler(this.buttonAutomatico_Click);
            // 
            // buttonRelay7VuotoApplicatore
            // 
            this.buttonRelay7VuotoApplicatore.Location = new System.Drawing.Point(683, 107);
            this.buttonRelay7VuotoApplicatore.Name = "buttonRelay7VuotoApplicatore";
            this.buttonRelay7VuotoApplicatore.Size = new System.Drawing.Size(152, 23);
            this.buttonRelay7VuotoApplicatore.TabIndex = 20;
            this.buttonRelay7VuotoApplicatore.Text = "7) Rele Vuoto Applicatore";
            this.buttonRelay7VuotoApplicatore.UseVisualStyleBackColor = true;
            this.buttonRelay7VuotoApplicatore.Click += new System.EventHandler(this.buttonRelay7VuotoApplicatore_Click);
            // 
            // labelStampaCompletaTimer
            // 
            this.labelStampaCompletaTimer.AutoSize = true;
            this.labelStampaCompletaTimer.Location = new System.Drawing.Point(271, 165);
            this.labelStampaCompletaTimer.Name = "labelStampaCompletaTimer";
            this.labelStampaCompletaTimer.Size = new System.Drawing.Size(116, 13);
            this.labelStampaCompletaTimer.TabIndex = 21;
            this.labelStampaCompletaTimer.Text = "Timer stampa completa";
            // 
            // FormStatoPlc
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(907, 544);
            this.Controls.Add(this.labelStampaCompletaTimer);
            this.Controls.Add(this.buttonRelay7VuotoApplicatore);
            this.Controls.Add(this.buttonAutomatico);
            this.Controls.Add(this.buttonTestIO);
            this.Controls.Add(this.buttonVRErroreTimerGoDown);
            this.Controls.Add(this.buttonVRErroreTimerGoUp);
            this.Controls.Add(this.buttonVRApplicatoreBusy);
            this.Controls.Add(this.buttonRelay6Allarme);
            this.Controls.Add(this.labelGoUpTimer);
            this.Controls.Add(this.labelGoDownTimer);
            this.Controls.Add(this.labelTimerApplicazione);
            this.Controls.Add(this.labelState);
            this.Controls.Add(this.buttonClose);
            this.Controls.Add(this.buttonRelay6ApplicatoreUp);
            this.Controls.Add(this.buttonRelay5ApplicatoreDown);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.buttonInput5VaschettPresente);
            this.Controls.Add(this.buttonInput4EttichettaPresente);
            this.Controls.Add(this.buttonInput3ApplicatorePosInf);
            this.Controls.Add(this.buttonInput2ApplicatorePosSuperiore);
            this.Controls.Add(this.buttonInput1Emmergenza);
            this.Name = "FormStatoPlc";
            this.Text = "FormStatoPlc";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormStatoPlc_FormClosing);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FormStatoPlc_FormClosed);
            this.Load += new System.EventHandler(this.FormStatoPlc_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonInput1Emmergenza;
        private System.Windows.Forms.Button buttonInput2ApplicatorePosSuperiore;
        private System.Windows.Forms.Button buttonInput3ApplicatorePosInf;
        private System.Windows.Forms.Button buttonInput4EttichettaPresente;
        private System.Windows.Forms.Button buttonInput5VaschettPresente;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button buttonRelay5ApplicatoreDown;
        private System.Windows.Forms.Button buttonRelay6ApplicatoreUp;
        private System.Windows.Forms.Button buttonClose;
        private System.Windows.Forms.Label labelState;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Label labelTimerApplicazione;
        private System.Windows.Forms.Label labelGoDownTimer;
        private System.Windows.Forms.Label labelGoUpTimer;
        private System.Windows.Forms.Button buttonRelay6Allarme;
        private System.Windows.Forms.Button buttonVRApplicatoreBusy;
        private System.Windows.Forms.Button buttonVRErroreTimerGoUp;
        private System.Windows.Forms.Button buttonVRErroreTimerGoDown;
        private System.Windows.Forms.Button buttonTestIO;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.Button buttonAutomatico;
        private System.Windows.Forms.Button buttonRelay7VuotoApplicatore;
        private System.Windows.Forms.Label labelStampaCompletaTimer;
    }
}