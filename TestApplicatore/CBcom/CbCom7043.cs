﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using TestApplicatore.CBcom.Exceptions;

namespace TestApplicatore.CBcom
{
    /// <summary>
    /// Implementazione del modulo CB 7060
    /// </summary>
    internal class CbCom7043 : CbComGeneric
    {
        private byte[] _buffer = new byte[100];

        public CbCom7043(SerialPortWithLock serialPort, int address) : base(serialPort, address)
        {
            //
            //  Il modulo ha solo 4 canali relè
            //
            if ((address < 0) || (address > 0xFF))
            {
                throw new ArgumentOutOfRangeException();
            }

            if (!SerialPort.IsOpen)
            {
                SerialPort.Open();
            }
        }

        /// <summary>
        /// Imposta un relè sulla scheda
        /// </summary>
        /// <param name="address"></param>
        /// <param name="channalN"></param>
        /// <param name="state"></param>
        public void SetValue(int channalN, bool state)
        {
            //
            //  Il modulo ha solo 4 canali relè
            //
            if ((channalN < 0) || (channalN > 0x0F))
            {
                throw new ArgumentOutOfRangeException();
            }

            char channelLetter = 'A';
            if (channalN > 7)
            {
                channelLetter = 'B';
                channalN -= 8;
            }

            string cmd = string.Format("#{0:X2}{1}{2:X1}{3:02}", Address, channelLetter, channalN, state ? "01" : "00");

            List<byte> cmdBuffer = new List<byte>();
            cmdBuffer.AddRange(Encoding.ASCII.GetBytes(cmd));
            cmdBuffer.AddRange(Encoding.ASCII.GetBytes(CalcCheckSum(cmd)));
            cmdBuffer.Add(Convert.ToByte('\r'));
            SerialPort.Write(cmdBuffer.ToArray(), 0, cmdBuffer.Count);

            Thread.Sleep(100);
            SerialPort.Read(_buffer, 0, _buffer.Length);

            // chiudo la seriale
            //    SerialPort.Close();

            string fullRespoce = Encoding.ASCII.GetString(_buffer).Trim('\0', '\r');

            if (fullRespoce.IndexOf(">") == -1)
            {
                throw new WrongMessageFormatException(cmdBuffer.ToArray(), _buffer);
            }
        }
    }
}