﻿using System;

namespace TestApplicatore.CBcom.Exceptions
{
    /// <summary>
    /// Wrong Message Format Exception
    /// </summary>
    [Serializable]
    public class WrongMessageFormatException : Exception
    {
        public WrongMessageFormatException()
        {
        }

        public WrongMessageFormatException(string message)
                : base(message)
        {
        }

        public WrongMessageFormatException(string message, Exception inner)
                : base(message, inner)
        {
        }

        public WrongMessageFormatException(byte[] outMessage, byte[] inMessage)
                    : base(string.Format("WrongMessageFormat out message:{0:X} in message{1:X}", outMessage, inMessage))

        {
        }

        /// <summary>
        /// In message
        /// </summary>
        public byte[] InMessage { get; set; }

        /// <summary>
        /// Out message
        /// </summary>
        public byte[] OutMessage { get; set; }
    }
}